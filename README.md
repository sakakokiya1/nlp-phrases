# nlp-phrase
根据nlp中互信息以及左右信息得到目标文件夹中的文本的重要短语，然后用solr建立索引。

##新功能列表
*	增加新功能3：增加 eshore.cn.it.main.LinesPhraser 提取短语的时候，如果所有文件是存放在一个文件里面，那么就用这个类，并且这个类可以处理每行以指定分隔符合分割的文件，只处理某一列，最后提取重要短语，保存结果到指定文件中
*	增加新功能2：增加 eshore.cn.it.main.DocsPhraser 目标文件夹中所有文件的短语提取，保存结果到指定文件中
*	增加新功能1：修改 eshore.cn.it.business.IndexAction 可以选择是否建立索引，是否将提取的文件保存到本地指定文本中。

配置类：eshore.cn.it.phrase.ActionConfig.java
里面可以配置需要建立提取关键短语的文本语料库等，请查看该类注释。
程序入口：eshore.cn.it.main
*	1.	IndexStart.java  在配置类中配置好参数之后，直接运行此程序，将会自动建立索引到Solr中，格式请查看对应类
*	2.	SearchStart.java 查询类，指定字符查询，最后保存到配置类中指定的位置。

具体程序运行结果，请测试即可。