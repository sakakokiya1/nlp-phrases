package eshore.cn.it.main;

import com.hankcs.hanlp.corpus.io.FolderWalker;
import com.hankcs.hanlp.phrase.MutualInformationEntropyPhraseExtractor;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.*;

import org.apache.commons.io.FileUtils;

/**
 * <summary>此类通过指定目标语料库地址，程序将根据互信息排名提取前面20个短语输出保存到指定目录中某个文件，
 * 同时给出所有词语在所有文档中出现次数的排名，保存到指定目录某个文件</summary>
 * <author>clebeg</author>
 * <email>clebeg@163.com</email>
 * <create-date>2015-04-10 10:17</create-date>
 */
public class DocsPhraser {
    private  String FOLDER = "F:\\广州亿迅科技有限公司工作记录\\瓜棚\\政府负面新闻";
    private  String PHRASES_FILE = "data/phrases_title.txt";
    private  String PHRASES_FREQUENT_FILE = "data/phrases_frequent.txt";
    public static void main(String[] args) throws Exception {
		//此处可以加入命令行处理代码，此处省略
    	DocsPhraser pt = new DocsPhraser();
    	pt.extract();
	}
    
    
    /**
     * 此类将从指定文件夹中的所有文档中依次抽取所有的关键词，然后输出到两个指定的文档中。
     * @author clebeg	2015-04-10
     * @throws Exception
     */
    public void extract() throws Exception {
    	//记录程序开始执行时间
    	long start = System.currentTimeMillis();
        
    	//打开需要提取短语的文件，以后新的数据可以删除此文件夹，添加新的数据。
    	List<File> fileList = FolderWalker.open(FOLDER);
    	
    	//此map保存每个文件中的排名前20的短语
        Map<String, String> phraseMap = new LinkedHashMap<String, String>();
        
        //此map记录每个单词出现的次数
        Map<String, Integer> phraseFrequent = new LinkedHashMap<String, Integer>();
        
        int i = 0;
        for (File file : fileList) {
        	//输出状态，现在执行到第几个文件了。
            System.out.print(++i + " / " + fileList.size() + " " + file.getName() + " ");
            String content = FileUtils.readFileToString(file, "GBK");
            //提取短语
            List<String> phraseList = MutualInformationEntropyPhraseExtractor.extract(content, 20);
            
            //提取关键词
            //List<String> keywordList = HanLP.extractKeyword(content, 10);
            
            //输出提取到的短语
            System.out.print(phraseList);
           // System.out.print(keywordList);
            //遍历所有短语
            for (String phrase : phraseList) {
            	//加入短语次数统计map
            	Integer number = phraseFrequent.get(phrase);
            	if(number == null) {
            		number = 1;
            	} else {
            		number += 1;
            	}
            	phraseFrequent.put(phrase, number);
            	
            	//加入文档单词统计map
            	String value = phraseMap.get(file.getName());
            	if (value == null)
            		phraseMap.put(file.getName(), phrase);
            	else {
            		value += "," + phrase;
            		phraseMap.put(file.getName(), value);
            	}
            }
            
//            for (String phrase : keywordList) {
//            	//加入短语次数统计map
//            	Integer number = phraseFrequent.get(phrase);
//            	if(number == null) {
//            		number = 1;
//            	} else {
//            		number += 1;
//            	}
//            	phraseFrequent.put(phrase, number);
//            	
//            	//加入文档单词统计map
//            	String value = phraseMap.get(file.getName());
//            	if (value == null)
//            		phraseMap.put(file.getName(), phrase);
//            	else {
//            		value += "," + phrase;
//            		phraseMap.put(file.getName(), value);
//            	}
//            }
            System.out.println();
        }
        long end = System.currentTimeMillis();
        phraseMap.put(fileList.size() + " 篇文档， 总大小： " + new File(FOLDER).getTotalSpace() + " bytes 总耗时.", (end - start) + "ms");
       
        
        //将数据写到文本
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(PHRASES_FILE)));
        
        //保存每个文本中出现的新词到指定文件里面。
        for (Map.Entry<String, String> entry : phraseMap.entrySet()) {
            bw.write(entry.getValue() + "\t" + entry.getKey());
            bw.newLine();
        }
        bw.close();
        
        bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(PHRASES_FREQUENT_FILE)));
        
        //注意下面是排序
        List<Map.Entry<String,Integer>> list = new ArrayList<Map.Entry<String,Integer>>(phraseFrequent.entrySet());
        ValueComparator vc = new ValueComparator();
        Collections.sort(list, vc);
        //保存每个单词出现在所有文档中的词频信息。
        for (Map.Entry<String, Integer> entry : list) {
            bw.write(entry.getKey() + "\t" + entry.getValue());
            bw.newLine();
        }
        bw.close();
    }
    
    //map排序接口，从大到小排序
    private class ValueComparator implements Comparator<Map.Entry<String, Integer>> {  
        public int compare(Map.Entry<String, Integer> mp1, Map.Entry<String, Integer> mp2) {  
            return mp2.getValue() - mp1.getValue();  
        }  
    }  
    
    
}
