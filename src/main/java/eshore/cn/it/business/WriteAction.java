package eshore.cn.it.business;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import eshore.cn.it.phrase.ActionConfig;

public class WriteAction {
	public static void writeData2File(
			String file, String line, boolean isAppend) {
		try {
			FileUtils.write(new File(file), line + System.lineSeparator(), ActionConfig.FILE_ENCODING, isAppend);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
